let preview = document.querySelector("#preview")
function enviar() {
    let input = document.querySelector('#input')
    let n = input.value

    if (n > 10 || n < 0) {
        alert('Nota inválida!')
        return
    }
    if (n == '') {
        alert('Espaço em branco!')
        return
    }

    input.value = ''
    let p = document.createElement('p')
    p.innerText = 'A nota é: ' + n
    soma = soma + Number(n)
    c = c + 1
    let preview = document.querySelector('#preview')
    preview.append(p)
}

function media() {
    let preview = document.querySelector('#preview')
    let media = soma / (c-1)
    let result = document.querySelector('#result')
    result.innerHTML = 'A média é: ' + media
    preview.innerHTML = ''
}

var c = 1
var soma = 0

let addButton = document.querySelector('#adicionar')
addButton.addEventListener('click', ()=>enviar(c,soma))

let calcButton = document.querySelector('#calcular')
calcButton.addEventListener('click', ()=>media(c,soma))
